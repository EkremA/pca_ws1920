# -*- coding: utf-8 -*-
"""
Created on Sun 17.11.19
@author: Ekrem
"""
import numpy as np
import matplotlib.pyplot as plt
import pylab as pb

tmain = np.array([465915, 12103509, 49358423, 1246432303, 4976684138])
tcomp = np.array([465354, 12090788, 49307718, 1245176193, 4971485212])
m = np.array([35, 175, 350, 1750, 3500])
N = np.array([100.,500.,1000.,5000.,10000.])

plt.loglog(N, tmain, 'o-', label="$t_{wall}$")
plt.loglog(N, N**2., 'o--', label="$N^{2}$")
plt.legend(loc="lower right")
plt.grid(True, which="minor")
plt.title("Scaling of heat-transfer program (sequential)")
plt.xlabel("grid-width $N$")
plt.ylabel("time[$\mu s$]")
pb.savefig("heat_bench.png", dpi=600)
plt.show()

