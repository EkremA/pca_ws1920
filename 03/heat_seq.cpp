/*
    ____  _________       _       ______________ ___   ____ 
   / __ \/ ____/   |     | |     / / ___<  / __ \__ \ / __ \
  / /_/ / /   / /| |_____| | /| / /\__ \/ / /_/ /_/ // / / /
 / ____/ /___/ ___ /_____/ |/ |/ /___/ / /\__, / __// /_/ / 
/_/    \____/_/  |_|     |__/|__//____/_//____/____/\____/  

				Prof. Dr. Ulrich Bruening
============================================================
						Exercise 3
				Heat transfer (sequential) 
============================================================
			An SoA (array of row-arrays) approach
============================================================
Group:	pra02
Names:	Andreas Georg Junge
		Ekrem Altuntop
===========================
*/

#include <iostream>	//cout
#include <cstdlib>	//atoi
#include <cmath>	//sqrt, abs
#include <chrono>	//high_resolution_clock

double next_xij(double x_i_j_t, double x_ip1_j_t, double x_im1_j_t, double x_i_jp1_t, double x_i_jm1_t)
{
	double phi = 6/25; 
	return x_i_j_t + phi * (-4.0*x_i_j_t + x_ip1_j_t + x_im1_j_t + x_i_jp1_t + x_i_jm1_t);
}

double distance(double dn, double dm)
{
	return std::sqrt(dn*dn + dm*dm);
}

int main(int argc, char* argv[])
{
	auto t_main_start = std::chrono::high_resolution_clock::now();

	//Predefinitions
	//----------------------------
	//General x-Value range
	double X_MAX = 127.0;
	double X_MIN = 0.0;
	//Grid
	int N = atoi(argv[1]);	//number of columns
	int M = N;				//number of rows
	//Circle
	int m = atoi(argv[2]);	//radius circle, note: int variable
	double H = atoi(argv[3]);	//init val of circle
	if (H<X_MIN || H>X_MAX)
	{
		std::cout << "H value is not passable. I needs to be in [0.0, 127.0]. Retry." << std::endl;
		return 1;	
	}
	int N0 = N/2; //if N is even => N/2-0>(N-1)-N/2, so not exactly in middle of grid
	int M0 = M/2;
	//Simulation and Computations
	int N_steps = 1000;	//time steps





	//Initialisation
	//=============================================
	//Grid
	//---------------------------
	//row major ordering for grid
	//allocations
	double ** grid = new double* [M];
	for(int i=0; i<M; i++)
		grid[i] = new double[N];
	//initialise
	for(int i=0; i<M; i++)
		for(int j=0; j<N; j++)
			grid[i][j] = 0.0; //this notation is cool


	//Circle heat
	//---------------------------
	if (M0+m>M-1 || N0+m>N-1)
	{
		std::cout << "Circle does not fit into grid. Change sizes and retry." << std::endl; 
		return 1;
	}
	for(int i=0; i<M; i++)
	{
		for(int j=0; j<N; j++)
		{
			double dist = distance(std::abs(N0-j), std::abs(M0-i));
			if (dist < m)
			{
				grid[i][j] = H;
			}
		}
	}


	
	//Computations and Simulation
	//=============================================
	//temporary grid to cache intermediate results
	double ** temp = new double* [M];
	for(int i=0; i<M; i++)
		temp[i] = new double[N];
	for(int i=0; i<M; i++)
		for(int j=0; j<N; j++)
			temp[i][j] = grid[i][j];

	//time-loop
	auto t_compute_start = std::chrono::high_resolution_clock::now(); 
	for(int t=0; t<N_steps; t++)
	{
		//grid-loops
		//Never update grid edges, they will stay 0.0
		for(int i=1; i<M-1; i++)
		{
			for(int j=1; j<N-1; j++)
			{
				//Compute iteration
				temp[i][j] = next_xij(grid[i][j], grid[i+1][j], grid[i-1][j], grid[i][j+1], grid[i][j-1]);
				//Check validity of values
				temp[i][j] = (temp[i][j]>X_MAX) ? X_MAX : temp[i][j];
				temp[i][j] = (temp[i][j]<X_MIN) ? X_MIN : temp[i][j];
			}
		}
		//Update actual grid array
		for(int i=1; i<M-1; i++)
			for(int j=1; j<N-1; j++)
				grid[i][j] = temp[i][j];
	}
	auto t_compute = std::chrono::high_resolution_clock::now() - t_compute_start;




	//Clean up
	//=============================================
	for(int i=0; i<M; i++)
		delete [] temp[i];
	delete [] temp;

	for(int i=0; i<M; i++)
		delete [] grid[i];
	delete [] grid;


	auto t_main = std::chrono::high_resolution_clock::now() - t_main_start;
	//Output
	//=============================================
	std::cout << "N:" << N << std::endl;
	std::cout << "M:" << M << std::endl;
	std::cout << "m:" << m << std::endl;
	std::cout << "H:" << H << std::endl;
	std::cout << "X_MAX:" << X_MAX << std::endl;
	std::cout << "X_MIN:" << X_MIN << std::endl;
	std::cout << "t_compute:" << std::chrono::duration_cast<std::chrono::microseconds>(t_compute).count() << std::endl;
	std::cout << "t_main:" << std::chrono::duration_cast<std::chrono::microseconds>(t_main).count() << std::endl;
	std::cout << std::endl;

	return 0;
}
