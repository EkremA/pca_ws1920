# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 15:05:31 2019

@author: Ekrem
"""

import numpy as np
import matplotlib.pyplot as plt
import pylab as pb
import pandas as pd
"""
Ninter, SizeInter, HaLaInter = np.loadtxt("pingpongInter.bench", usecols=(1,2,4), unpack=True)
Nintra, SizeIntra, HaLaIntra = np.loadtxt("pingpongIntra.bench", usecols=(1,2,4), unpack=True)

if (np.all(Nintra==Ninter)==False):
    raise Exception("Ninter und Nintra sind nicht gleich")
#plt.semilogy(Ninter, HaLaInter, Nintra, HaLaIntra)
plt.plot(SizeInter, HaLaInter, '.-',  label="Internode") 
plt.plot(SizeIntra, HaLaIntra, '.-', label="Intranode")

plt.legend(loc="best")
plt.grid(True)
plt.title("Half-Round-Latency of MPI-Pingpong")
plt.xlabel("N")
plt.ylabel("Time [s]")
pb.savefig("pingpongInterIntra.png", dpi=400)
plt.show()
"""

def readBenchFile(filename):
    return pd.read_csv(filename, sep=" ", header=None, names=["numRanks", 
                                                                          "N", 
                                                                          "msgSize", 
                                                                          "time", 
                                                                          "hrtl", 
                                                                          "comment"])

def getMeanStd(dataframe, N_val):
    mask = dataframe["N"]==N_val
    hrtlData = dataframe["hrtl"][mask]
    return np.mean(hrtlData), np.std(hrtlData)

def getAllStats(dataframe):
    means = np.array([])
    stds = np.array([])
    for n in dataframe["N"].unique():
        mask = dataframe["N"]==n
        means = np.append(means, dataframe["hrtl"][mask].mean())
        stds = np.append(stds, dataframe["hrtl"][mask].std())
    return means, stds

ppIntra = readBenchFile("pingpongIntra.bench")
ppInter = readBenchFile("pingpongInter.bench")
peIntra = readBenchFile("pingexIntra.bench")
peInter = readBenchFile("pingexInter.bench")

benchList = [ppIntra, ppInter, peIntra, peInter]

#msgSize und N sind isomorph deshalb kann ich das it msgSize hier machen
plotX = "N" #"msgSize"
mas = 3 #markersize 
lis = ":" 
#Ping Pong
plt.errorbar(ppInter[plotX].unique(), getAllStats(ppInter)[0], yerr=getAllStats(ppInter)[1], ls=lis, marker="o", markersize=mas, capsize=3, color= "blue", label="Pingpong Internode")
plt.errorbar(ppIntra[plotX].unique(), getAllStats(ppIntra)[0], yerr=getAllStats(ppIntra)[1], ls=lis, marker="o", markersize=mas, capsize=3, color= "cyan", label="Pingpong Intranode")
plt.errorbar(peInter[plotX].unique(), getAllStats(peInter)[0], yerr=getAllStats(peInter)[1], ls=lis, marker="o", markersize=mas, capsize=3, color= "red", label="Pingexchange Internode")
plt.errorbar(peIntra[plotX].unique(), getAllStats(peIntra)[0], yerr=getAllStats(peIntra)[1], ls=lis, marker="o", markersize=mas, capsize=3, color= "orange", label="Pingexchange Intranode")
plt.yscale("log")
plt.xticks(range(0,21))
plt.title("Half-Round-Latency of MPI-Pingpong")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.legend(loc="best")
plt.grid(True)
plt.grid(True, which="minor")
pb.savefig("pingpongPingexInterIntra.png", dpi=400)
plt.show()
plt.close()

#Ping Exchange
plt.errorbar(peInter[plotX].unique(), getAllStats(peInter)[0], yerr=getAllStats(peInter)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="Internode")
plt.errorbar(peIntra[plotX].unique(), getAllStats(peIntra)[0], yerr=getAllStats(peIntra)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="Intranode")
plt.yscale("log")
plt.xticks(range(0,21))
plt.title("Half-Round-Latency of MPI-Pingexchange")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.legend(loc="best")
plt.grid(True)
plt.grid(True, which="minor")
pb.savefig("pingexInterIntra.png", dpi=400)
plt.show()