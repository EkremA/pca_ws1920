#include <iostream>
#include <cstdlib> 
#include <mpi.h>

int main(int argc, char* argv[])
{
	//int num_ranks = atoi(argv[1]);
	MPI_Init(&argc, &argv);

	
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	std::cout << rank << " says hello world!" << std::endl;
	
	int size = -2;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	std::cout <<  "Size is " << size << std::endl;
	
	int temp=0;
	if(rank==0)
	{
		for(int i=0; i<10000000; i++)
			temp += temp*5 / 7 % 3 / 2 + 2;
	}
	else
	{
		for(int i=0; i<10000000; i++)
			temp += temp*5 / 7 % 3 / 2 + 2;		
	}

	MPI_Finalize();
	return 0;
}