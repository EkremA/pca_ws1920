# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 15:05:31 2019

@author: Ekrem
"""

import numpy as np
import matplotlib.pyplot as plt
import pylab as pb
import pandas as pd

def readBenchFile(filename):
    return pd.read_csv(filename, sep=" ", header=None, names=["numRanks", 
                                                                          "N", 
                                                                          "msgSize", 
                                                                          "time", 
                                                                          "hrtl", 
                                                                          "comment"])

def getMeanStd(dataframe, N_val):
    mask = dataframe["N"]==N_val
    hrtlData = dataframe["hrtl"][mask]
    return np.mean(hrtlData), np.std(hrtlData)

def getAllStats(dataframe):
    means = np.array([])
    stds = np.array([])
    for n in dataframe["N"].unique():
        mask = dataframe["N"]==n
        means = np.append(means, dataframe["hrtl"][mask].mean())
        stds = np.append(stds, dataframe["hrtl"][mask].std())
    return means, stds


ppIntra_0 = readBenchFile("pingpongIntra_numa0.bench")
ppIntra_1 = readBenchFile("pingpongIntra_numa1.bench")
ppInter_0 = readBenchFile("pingpongInter_numa0.bench")
ppInter_1 = readBenchFile("pingpongInter_numa1.bench")

peIntra_0 = readBenchFile("pingexIntra_numa0.bench")
peIntra_1 = readBenchFile("pingexIntra_numa1.bench")
peInter_0 = readBenchFile("pingexInter_numa0.bench")
peInter_1 = readBenchFile("pingexInter_numa1.bench")


#msgSize und N sind isomorph deshalb kann ich das it msgSize hier machen
plotX = "N" #"msgSize"
mas = 3 #markersize 
lis = ":" 
#Ping Pong
plt.errorbar(ppIntra_0[plotX].unique(), getAllStats(ppIntra_0)[0], yerr=getAllStats(ppIntra_0)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="cpunodebind=0")
plt.errorbar(ppIntra_1[plotX].unique(), getAllStats(ppIntra_1)[0], yerr=getAllStats(ppIntra_1)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="cpunodebind=1")
plt.yscale("log")
plt.xticks(range(0,21))
plt.title("Half-Round-Latency of Intranode-MPI-Pingpong: NUMA Nodebinding")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.legend(loc="best")
plt.grid(True)
plt.grid(True, which="minor")
pb.savefig("pingpongIntraNuma.png", dpi=400)
plt.show()
plt.close()

plt.errorbar(ppInter_0[plotX].unique(), getAllStats(ppInter_0)[0], yerr=getAllStats(ppInter_0)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="cpunodebind=0")
plt.errorbar(ppInter_1[plotX].unique(), getAllStats(ppInter_1)[0], yerr=getAllStats(ppInter_1)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="cpunodebind=1")
plt.yscale("log")
plt.xticks(range(0,21))
plt.title("Half-Round-Latency of Internode-MPI-Pingpong: NUMA Nodebinding")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.legend(loc="best")
plt.grid(True)
plt.grid(True, which="minor")
pb.savefig("pingpongInterNuma.png", dpi=400)
plt.show()
plt.close()

plt.errorbar(peIntra_0[plotX].unique(), getAllStats(peIntra_0)[0], yerr=getAllStats(peIntra_0)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="cpunodebind=0")
plt.errorbar(peIntra_1[plotX].unique(), getAllStats(peIntra_1)[0], yerr=getAllStats(peIntra_1)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="cpunodebind=1")
plt.yscale("log")
plt.xticks(range(0,21))
plt.title("Half-Round-Latency of Intranode-MPI-Pingexchange: NUMA Nodebinding")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.legend(loc="best")
plt.grid(True)
plt.grid(True, which="minor")
pb.savefig("pingexIntraNuma.png", dpi=400)
plt.show()
plt.close()

plt.errorbar(peInter_0[plotX].unique(), getAllStats(peInter_0)[0], yerr=getAllStats(peInter_0)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="cpunodebind=0")
plt.errorbar(peInter_1[plotX].unique(), getAllStats(peInter_1)[0], yerr=getAllStats(peInter_1)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="cpunodebind=1")
plt.yscale("log")
plt.xticks(range(0,21))
plt.title("Half-Round-Latency of Internode-MPI-Pingexchange: NUMA Nodebinding")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.legend(loc="best")
plt.grid(True)
plt.grid(True, which="minor")
pb.savefig("pingexInterNuma.png", dpi=400)
plt.show()
plt.close()

"""
#Ping Exchange
plt.errorbar(peInter[plotX].unique(), getAllStats(peInter)[0], yerr=getAllStats(peInter)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="Internode")
plt.errorbar(peIntra[plotX].unique(), getAllStats(peIntra)[0], yerr=getAllStats(peIntra)[1], ls=lis, marker="o", markersize=mas, capsize=3, label="Intranode")
plt.yscale("log")
plt.xticks(range(0,21))
plt.title("Half-Round-Latency of MPI-Pingexchange")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.legend(loc="best")
plt.grid(True)
plt.grid(True, which="minor")
pb.savefig("pingexInterIntra.png", dpi=400)
plt.show()
"""