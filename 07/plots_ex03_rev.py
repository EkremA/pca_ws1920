# -*- coding: utf-8 -*-
"""
Created on Thu Jan 09 21:51:04 2020

@author: Ekrem
"""

import numpy as np
import matplotlib.pyplot as plt
import pylab as pb
import pandas as pd

def readBenchFile(filename):
    return pd.read_csv(filename, sep=" ", header=None, names=["numRanks", 
                                                                          "N", 
                                                                          "msgSize", 
                                                                          "time", 
                                                                          "hrtl", 
                                                                          "comment"])

def getMeanStd(dataframe, N_val):
    mask = dataframe["N"]==N_val
    hrtlData = dataframe["hrtl"][mask]
    return np.mean(hrtlData), np.std(hrtlData)

def getAllStats(dataframe):
    means = np.array([])
    stds = np.array([])
    for n in dataframe["N"].unique():
        mask = dataframe["N"]==n
        means = np.append(means, dataframe["hrtl"][mask].mean())
        stds = np.append(stds, dataframe["hrtl"][mask].std())
    return means, stds


ppIntra = readBenchFile("pingpongIntra.bench")
ppInter = readBenchFile("pingpongInter.bench")

peIntra = readBenchFile("pingexIntra.bench")
peInter = readBenchFile("pingexInter.bench")

ppIntra_0 = readBenchFile("pingpongIntra_numa0.bench")
ppIntra_1 = readBenchFile("pingpongIntra_numa1.bench")
ppInter_0 = readBenchFile("pingpongInter_numa0.bench")
ppInter_1 = readBenchFile("pingpongInter_numa1.bench")

peIntra_0 = readBenchFile("pingexIntra_numa0.bench")
peIntra_1 = readBenchFile("pingexIntra_numa1.bench")
peInter_0 = readBenchFile("pingexInter_numa0.bench")
peInter_1 = readBenchFile("pingexInter_numa1.bench")


mas = 3 #markersize 
lis = ":" 
plt.errorbar(ppInter["N"].unique(), getAllStats(ppInter)[0], yerr=getAllStats(ppInter)[1], marker=mas, linestyle=lis, capsize=2, label="no numactl")
plt.errorbar(ppInter["N"].unique(), getAllStats(ppInter_0)[0], yerr=getAllStats(ppInter_0)[1], marker=mas, linestyle=lis, capsize=2,label="numactl --cpunodebind=0")
plt.errorbar(ppInter["N"].unique(), getAllStats(ppInter_1)[0], yerr=getAllStats(ppInter_1)[1], marker=mas, linestyle=lis, capsize=2,label="numactl --cpunodebind=1")
plt.legend(loc="best")
plt.yscale("log")
plt.grid(True)
plt.grid(True, which="minor")
plt.title("Half-Round-Latency of Internode MPI-Pingpong: NUMA-Nodebinding")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.xticks(range(0,21))
pb.savefig("pingpongInter_numaComp.png", dpi=400)
plt.show()
plt.close()


plt.errorbar(ppIntra["N"].unique(), getAllStats(ppIntra)[0], yerr=getAllStats(ppIntra)[1], marker=mas, linestyle=lis, capsize=2,label="no numactl")
plt.errorbar(ppIntra["N"].unique(), getAllStats(ppIntra_0)[0], yerr=getAllStats(ppIntra_0)[1], marker=mas, linestyle=lis, capsize=2,label="numactl --cpunodebind=0")
plt.errorbar(ppIntra["N"].unique(), getAllStats(ppIntra_1)[0], yerr=getAllStats(ppIntra_1)[1], marker=mas, linestyle=lis, capsize=2,label="numactl --cpunodebind=1")
plt.legend(loc="best")
plt.yscale("log")
plt.grid(True)
plt.grid(True, which="minor")
plt.title("Half-Round-Latency of Intranode MPI-Pingpong: NUMA-Nodebinding")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.xticks(range(0,21))
pb.savefig("pingpongIntra_numaComp.png", dpi=400)
plt.show()
plt.close()


plt.errorbar(peInter["N"].unique(), getAllStats(peInter)[0], yerr=getAllStats(peInter)[1], marker=mas, linestyle=lis, capsize=2,label="no numactl")
plt.errorbar(peInter["N"].unique(), getAllStats(peInter_0)[0], yerr=getAllStats(peInter_0)[1], marker=mas, linestyle=lis, capsize=2,label="numactl --cpunodebind=0")
plt.errorbar(peInter["N"].unique(), getAllStats(peInter_1)[0], yerr=getAllStats(peInter_1)[1], marker=mas, linestyle=lis, capsize=2,label="numactl --cpunodebind=1")
plt.legend(loc="best")
plt.yscale("log")
plt.grid(True)
plt.grid(True, which="minor")
plt.title("Half-Round-Latency Internode MPI-Pingexchange: NUMA-Nodebinding")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.xticks(range(0,21))
pb.savefig("pingexInter_numaComp.png", dpi=400)
plt.show()
plt.close()

plt.errorbar(peIntra["N"].unique(), getAllStats(peIntra)[0], yerr=getAllStats(peIntra)[1], marker=mas, linestyle=lis, capsize=2,label="no numactl")
plt.errorbar(peIntra["N"].unique(), getAllStats(peIntra_0)[0], yerr=getAllStats(peIntra_0)[1], marker=mas, linestyle=lis, capsize=2,label="numactl --cpunodebind=0")
plt.errorbar(peIntra["N"].unique(), getAllStats(peIntra_1)[0], yerr=getAllStats(peIntra_1)[1], marker=mas, linestyle=lis, capsize=2,label="numactl --cpunodebind=1")
plt.legend(loc="best")
plt.yscale("log")
plt.grid(True)
plt.grid(True, which="minor")
plt.title("Half-Round-Latency Intranode MPI-Pingexchange: NUMA-Nodebinding")
plt.xlabel("N")
plt.ylabel("Time [s]")
plt.xticks(range(0,21))
pb.savefig("pingexIntra_numaComp.png", dpi=400)
plt.show()
plt.close()