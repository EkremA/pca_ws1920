#!/bin/bash

#	Name of program
#SBATCH --job-name=pingpong

#	Number of nodes used
#SBATCH --nodes=1

#	Total number of processes
#SBATCH --ntasks=2

#	Partition used on PEAC
#SBATCH --partition=bench

#	Write results into the same file
#SBATCH --open-mode=append
#SBATCH --output=pingpongIntra_numa0.bench


#Unload everything first to clean up
module purge > /dev/null 2>&1

#Load needed modules to run program
module load mpi

#Go through all the parameters needed
START=0
END=21
for ((i=START; i<END; i++)); do
	for ((j=0; j<5; j++)); do
		mpirun -mca btl tcp,self -n 2 -N 2 --oversubscribe numactl --cpunodebind=0 -- ./pingpong $i
		wait
	done
done