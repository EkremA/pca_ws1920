#include <iostream>
#include <cstdlib>
#include <cmath>
#include <mpi.h>

int main(int argc, char* argv[])
{
	int N = atoi(argv[1]);
	int numBytes = (int) pow(2.0, (double) N); 	//Is the number of elements alright? --> Rounding and stuff??
	double time = 0.0;

	MPI_Init(&argc, &argv);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);




	int size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	if(size!=2)
	{
		std::cout << "Error: This program is intended for exactly 2 processes. Relaunch please." << std::endl;
		return 1;
	}


	//We send arrays of unsigned chars around because minimal size is 1 Byte
	//+1 to make sure there is enough elements in case of previous weird rounding errors
	unsigned char * arr = new unsigned char[numBytes]; //there should be a more elegant way of doing it. Bitshifts maybe? 
	for(int i=0; i<numBytes; i++)
		arr[i] = 1;

	if(rank==0)
	{
		//ping
		double start = MPI_Wtime();	//measuring on only one of the processes here
		MPI_Send(arr, numBytes, MPI::UNSIGNED_CHAR, 1, 0, MPI_COMM_WORLD);
		MPI_Recv(arr, numBytes, MPI::UNSIGNED_CHAR, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		time = MPI_Wtime() - start;
	}
	else
	{
		//pong
		MPI_Recv(arr, numBytes, MPI::UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Send(arr, numBytes, MPI::UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);
	}


	if (rank==0)
	{
		//	numRanks ParamN MessageSizeInBytes	TimeNeeded	halfroundtriplatency	Comment
		std::cout << size << " " <<  N << " " << numBytes << " " << time << " " << time/2.0 << " " << "pingpong" << std::endl;
	}



	delete [] arr;
	MPI_Finalize();

	return 0;
}