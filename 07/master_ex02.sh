#!/bin/bash

#Ping Pong
for bfile in pingpongInternode.sh pingpongIntranode.sh
do
	sbatch $bfile
	sleep 1
	wait
done

#Ping-Exchange
for bfile in pingexInternode.sh pingexIntranode.sh
do
	sbatch $bfile
	sleep 1
	wait
done

