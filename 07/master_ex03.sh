#!/bin/bash

#Ping Pong
for bfile in pingpongInternode_numa0.sh pingpongInternode_numa1.sh pingpongIntranode_numa0.sh pingpongIntranode_numa1.sh
do
	sbatch $bfile
	sleep 1
	wait
done

#Ping-Exchange
for bfile in pingexInternode_numa0.sh pingexInternode_numa1.sh pingexIntranode_numa0.sh pingexIntranode_numa1.sh
do
	sbatch $bfile
	sleep 1
	wait
done

