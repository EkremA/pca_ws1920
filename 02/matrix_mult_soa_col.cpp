/*
    ____  _________       _       ______________ ___   ____ 
   / __ \/ ____/   |     | |     / / ___<  / __ \__ \ / __ \
  / /_/ / /   / /| |_____| | /| / /\__ \/ / /_/ /_/ // / / /
 / ____/ /___/ ___ /_____/ |/ |/ /___/ / /\__, / __// /_/ / 
/_/    \____/_/  |_|     |__/|__//____/_//____/____/\____/  

				Prof. Dr. Ulrich Bruening
============================================================
						Exercise 2
				Matrix multiplications
============================================================
			An SoA (array of column-arrays) approach
============================================================
Group:	pra02
Names:	Andreas Georg Junge
		Ekrem Altuntop
===========================
*/

#include <chrono>	//high_resolution_clock
#include <cstdio>	//printf
#include <cstdlib>	//atoi, srand, rand
#include <limits>	//numeric limits
#include <iostream> //cout

void setY_colwise(int M, int N, double ** A, double * x, double * y)
{
	/*
	requires y to be zero-vector
	*/
	for(int j=0; j<N; j++) //finish each column before moving on
	{
		for(int i=0; i<M; i++)
		{
			y[i] += A[i][j] * x[j];
		}
	}
}


auto start = std::chrono::high_resolution_clock::now();

int main(int argc, char* argv[])
{
	int M = atoi(argv[1]);
	int N = atoi(argv[2]);

	double *xj = new double[N];
	double *yi = new double[M];
	double **aij = new double*[N]; //an array of the COLS as arrays
	for(int j=0; j<N; j++)
		aij[j] = new double[M];


	//initialise xj and aij as randoms
	srand((std::chrono::high_resolution_clock::now()-start).count()); //set random seed
	for(int j=0; j<N; j++)
		xj[j] = (double) (rand() % (int) std::numeric_limits<double>::max());
	for(int j=0; j<N; j++)
	{
		for(int i=0; i<M; i++)
		{
			aij[i][j] = (double) (rand() % (int) std::numeric_limits<double>::max());
		}
	}
	//init yi as 0 for later
	for(int i=0; i<M; i++)
		yi[i] = 0.0;

	auto start_tcol = std::chrono::high_resolution_clock::now();
	setY_colwise(M, N, aij, xj, yi);
	auto duration_tcol = (std::chrono::high_resolution_clock::now()-start_tcol);


	std::cout << "t_col:" << std::chrono::duration_cast<std::chrono::microseconds>(duration_tcol).count() << std::endl; 

	delete [] xj;
	delete [] yi;
	for(int j=0; j<N; j++)
		delete [] aij[j];
	delete [] aij;

	return 0;
}