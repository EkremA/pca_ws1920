/*
    ____  _________       _       ______________ ___   ____ 
   / __ \/ ____/   |     | |     / / ___<  / __ \__ \ / __ \
  / /_/ / /   / /| |_____| | /| / /\__ \/ / /_/ /_/ // / / /
 / ____/ /___/ ___ /_____/ |/ |/ /___/ / /\__, / __// /_/ / 
/_/    \____/_/  |_|     |__/|__//____/_//____/____/\____/  

				Prof. Dr. Ulrich Bruening
============================================================
						Exercise 2
				Matrix multiplications
============================================================
			An SoA (array of row-arrays) approach
============================================================
Group:	pra02
Names:	Andreas Georg Junge
		Ekrem Altuntop
===========================
*/

#include <chrono>	//high_resolution_clock
#include <cstdio>	//printf
#include <cstdlib>	//atoi, srand, rand
#include <limits>	//numeric limits
#include <iostream> //cout

void setY_rowwise(int M, int N, double ** A, double * x, double * y)
{
	//get a row completely done before going to the next one
	for(int i=0; i<M; i++) 
	{ 
		for(int j=0; j<N; j++)
		{
			y[i] += A[i][j] * x[j];
		}
	}
}


auto start = std::chrono::high_resolution_clock::now();

int main(int argc, char* argv[])
{
	int M = atoi(argv[1]);
	int N = atoi(argv[2]);

	double *xj = new double[N];
	double *yi = new double[M];
	double **aij = new double*[M]; //an array of the ROWS as arrays
	for(int i=0; i<M; i++)
		aij[i] = new double[N];


	srand((std::chrono::high_resolution_clock::now()-start).count()); //set random seed
	

	for(int j=0; j<N; j++)
	{
		xj[j] = (double) (rand() % (int) std::numeric_limits<double>::max());
		for(int i=0; i<M; i++)
		{
			aij[i][j] = (double) (rand() % (int) std::numeric_limits<double>::max());
		}
	}
	for(int i=0; i<M; i++)
		yi[i] = 0.0; //initialise as zero


	auto start_trow = std::chrono::high_resolution_clock::now();
	setY_rowwise(M, N, aij, xj, yi);
	auto duration_trow = (std::chrono::high_resolution_clock::now()-start_trow);


	std::cout << "t_row:" << std::chrono::duration_cast<std::chrono::milliseconds>(duration_trow).count() << std::endl; 

	delete [] xj;
	delete [] yi;
	for(int i=0; i<M; i++)
		delete [] aij[i];
	delete [] aij;

	return 0;
}