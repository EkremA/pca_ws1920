# -*- coding: utf-8 -*-
"""
Created on Sat Nov 02 18:22:51 2019

@author: Ekrem
"""

import numpy as np
import matplotlib.pyplot as plt
import pylab as pb

M = np.array([10,100,500,1000,5000,10000])
N = np.copy(M)
trow = np.array([0.00000001,0.00000001,2,8,233,964], dtype=float)
tcol = np.array([1,124,3429,23809,974334,4181640], dtype=float)
Ratio = tcol/trow

plt.loglog(M, trow, 'o-', label="t_row")
plt.loglog(M, tcol, 'o-', label="t_col")
plt.loglog(M, Ratio, 'o-', label="Verhaeltnis t_col/t_row")
plt.legend()
plt.title("Speicherzugriff: Zeilenweise vs. Spaltenweise")
plt.xlabel("M bzw. N")
plt.ylabel("Zeit Matrixmult. [us]")
plt.grid(True)
pb.savefig("ergebnis_zeiten.png", dpi=600)
plt.show()