/*
    ____  _________       _       ______________ ___   ____ 
   / __ \/ ____/   |     | |     / / ___<  / __ \__ \ / __ \
  / /_/ / /   / /| |_____| | /| / /\__ \/ / /_/ /_/ // / / /
 / ____/ /___/ ___ /_____/ |/ |/ /___/ / /\__, / __// /_/ / 
/_/    \____/_/  |_|     |__/|__//____/_//____/____/\____/  

                Prof. Dr. Ulrich Bruening
============================================================
                        Exercise 6
                Heat transfer (sequential) 
============================================================
                Pi-approximation using OpenMP
============================================================
Group:  pra02
Names:  Andreas Georg Junge
                Ekrem Altuntop
===========================
*/

#include <iostream> //std::cout
#include <cstdlib> //atoi
#include <cmath> //pow
#include <chrono> //clock
#include <omp.h>

int main(int argc, char * argv[])
{
        typedef typename std::chrono::high_resolution_clock clock;
        typedef typename clock::time_point time_point;

        time_point init_time_start;
        time_point init_time_end;
        time_point comp_time_start;
        time_point comp_time_end;
        time_point tot_time_start;
        time_point tot_time_end;


        tot_time_start = init_time_start = clock::now();
        int N = atoi(argv[1]); //erstes Kommando-Zeilen ist Zahl N aus der Summe fuer Pi
        int Nthreads = atoi(argv[2]);
        double dx = 1.0 / N; //(hier: implizite Typ-Konversion) Unterteilen des Integrations-Intervalls fuer einzelne Rechtecke
        double dx_h = 0.5 * dx; //fuer Loop spaeter, zur Vereinfachung
        double sum = 0.0; //Wird Ergebnis beinhalten
        init_time_end = clock::now();
        comp_time_start = clock::now();


        omp_set_num_threads(Nthreads);
        double comp_start_omp = omp_get_wtime();
        #pragma omp parallel for schedule(static, N/Nthreads) reduction(+:sum)
                for (int i=0; i<N; i++) //Summe zur Approximation mit xi- und F(xi)-Berechnung
                {
                        sum += 4.0 / (1.0 + pow(dx_h + i*dx, 2.0)) * dx; //auch hier implizite Typ-Konversion
                }
        double comp_time_omp = omp_get_wtime() - comp_start_omp;  
        double omp_clock_tick = omp_get_wtick();

        comp_time_end = tot_time_end = clock::now();


        std::cout.precision(17); //alle Nachkommastellen von double-Variablen ausgeben
        std::cout << "Pi-Value:"  << sum << std::endl;
        std::cout << "Total Time:" << std::chrono::duration_cast<std::chrono::nanoseconds>(tot_time_end-tot_time_start).count() << std::endl;
        std::cout << "Init Time:" << std::chrono::duration_cast<std::chrono::nanoseconds>(init_time_end-init_time_start).count() << std::endl;
        std::cout << "Comp Time:" << comp_time_omp << std::endl;
        std::cout << "OMP Clock Tick:" << omp_clock_tick << std::endl;



        return 0;
}
