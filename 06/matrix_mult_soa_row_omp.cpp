/*
    ____  _________       _       ______________ ___   ____ 
   / __ \/ ____/   |     | |     / / ___<  / __ \__ \ / __ \
  / /_/ / /   / /| |_____| | /| / /\__ \/ / /_/ /_/ // / / /
 / ____/ /___/ ___ /_____/ |/ |/ /___/ / /\__, / __// /_/ / 
/_/    \____/_/  |_|     |__/|__//____/_//____/____/\____/  

				Prof. Dr. Ulrich Bruening
============================================================
						Exercise 2
				Matrix multiplications
============================================================
			An SoA (array of row-arrays) approach
============================================================
Group:	pra02
Names:	Andreas Georg Junge
		Ekrem Altuntop
===========================
*/

#include <chrono>	//high_resolution_clock
#include <cstdio>	//printf
#include <cstdlib>	//atoi, srand, rand
#include <limits>	//numeric limits
#include <iostream> //cout
#include <omp.h>



void setY_rowwise(int M, int N, double ** A, double * x, double * y, int num_threads);

auto start = std::chrono::high_resolution_clock::now();

int main(int argc, char* argv[])
{
	int M = atoi(argv[1]);
	int N = atoi(argv[2]);

	double *xj = new double[N];
	double *yi = new double[M];
	double **aij = new double * [M]; //an array of the ROWS as arrays


	int Nthreads = atoi(argv[3]);
	omp_set_num_threads(Nthreads);
	#pragma omp parallel for schedule(static, N/Nthreads)
	for(int i=0; i<M; i++)
		aij[i] = new double[N];


	srand((std::chrono::high_resolution_clock::now()-start).count()); //set random seed
	

	#pragma omp parallel for schedule(static, N/Nthreads) 
	for(int j=0; j<N; j++)
		xj[j] = (double) (rand() % (int) std::numeric_limits<double>::max());

	#pragma omp for collapse(2)
	for(int j=0; j<N; j++)
	{
		for(int i=0; i<M; i++)
		{
			aij[i][j] = (double) (rand() % (int) std::numeric_limits<double>::max());
		}
	}

	#pragma omp parallel for schedule(static, M/Nthreads)
	for(int i=0; i<M; i++)
		yi[i] = 0.0; //initialise as zero


	//auto start_trow = std::chrono::high_resolution_clock::now();
	setY_rowwise(M, N, aij, xj, yi, Nthreads);
	//auto duration_trow = (std::chrono::high_resolution_clock::now()-start_trow);


	//std::cout << "t_row:" << std::chrono::duration_cast<std::chrono::milliseconds>(duration_trow).count() << std::endl; 

	delete [] xj;
	delete [] yi;
	for(int i=0; i<M; i++)
		delete [] aij[i];
	delete [] aij;

	return 0;
}


void setY_rowwise(int M, int N, double ** A, double * x, double * y, int num_threads)
{
	//get a row completely done before going to the next one
	double start = omp_get_wtime();
	#pragma omp parallel for collapse(2) schedule(static, N/num_threads)
	for(int i=0; i<M; i++) 
	{ 
		for(int j=0; j<N; j++)
		{
			y[i] += A[i][j] * x[j];
		}
	}
	double trow_omp = omp_get_wtime() - start;
	std::cout << "t_row:" << trow_omp << std::endl;
	std::cout << "clocktick:" << omp_get_wtick() << std::endl;
}
