#include <omp.h>
#include <iostream>
#include <stdlib.h>
int main()
{
	//omp_set_num_threads(4);

	#pragma omp parallel for schedule(static, 5)
		for(int i=0; i<10; i++)
			{
			double b = 10+1-2/100;
			std::cout << omp_get_thread_num() << "Hello World!" << std::endl;
			}
	return 0;
}
