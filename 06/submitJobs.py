"""
Start in a directory with only the z1_a1 executable and this script.
"""
import numpy as np
import subprocess as sp
import time
import math
import matplotlib.pyplot as plt
import pylab as pb


Npi = 10.0 ** np.arange(2, 7)
Nthreads = np.array([1,2,4,8,16,32])



print("Submitting pi approx jobs now...")
for prec in Npi:
	for threads in Nthreads:
		outFile = "piApprox_%s_%s"%(str(prec), str(threads))
		sp.call("sbatch -p bench -n 1 -o %s --wrap \"./pi_omp %s %s\" " %(str(outFile), str(prec), str(threads)), shell=True)
		time.sleep(1) #Entlastung fuer Slurm
print("Done.")