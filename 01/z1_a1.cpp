/*
UEBUNGSBLATT 1
AUFGABE 1
==============
Gruppen-Id: pra02
Namen: Andreas Georg Junge, Ekrem Altuntop
*/

#include <iostream> //std::cout
#include <cstdlib> //atoi
#include <cmath> //pow
#include <chrono> //clock

int main(int argc, char * argv[])
{
        typedef typename std::chrono::high_resolution_clock clock;
        typedef typename clock::time_point time_point;

        time_point init_time_start;
        time_point init_time_end;
        time_point comp_time_start;
        time_point comp_time_end;
        time_point tot_time_start;
        time_point tot_time_end;


        tot_time_start = init_time_start = clock::now();
        int N = atoi(argv[1]); //erstes Kommando-Zeilen ist Zahl N aus der Summe fuer Pi
        double dx = 1.0 / N; //(hier: implizite Typ-Konversion) Unterteilen des Integrations-Intervalls fuer einzelne Rechtecke
        double dx_h = 0.5 * dx; //fuer Loop spaeter, zur Vereinfachung
        double sum = 0.0; //Wird Ergebnis beinhalten
        init_time_end = clock::now();

        comp_time_start = clock::now();
        for (int i=0; i<N; i++) //Summe zur Approximation mit xi- und F(xi)-Berechnung
        {
                sum += 4.0 / (1.0 + pow(dx_h + i*dx, 2.0)) * dx; //auch hier implizite Typ-Konversion
        }
        comp_time_end = tot_time_end = clock::now();


        std::cout.precision(17); //alle Nachkommastellen von double-Variablen ausgeben
        std::cout << "Pi-Value:"  << sum << std::endl;
        std::cout << "Total Time:" << std::chrono::duration_cast<std::chrono::nanoseconds>(tot_time_end-tot_time_start).count() << std::endl;
        std::cout << "Init Time:" << std::chrono::duration_cast<std::chrono::nanoseconds>(init_time_end-init_time_start).count() << std::endl;
        std::cout << "Comp Time:" << std::chrono::duration_cast<std::chrono::nanoseconds>(comp_time_end-comp_time_start).count() << std::endl;



        return 0;
}
