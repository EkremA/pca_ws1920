"""
UEBUNGSBLATT 1
AUFGABE 1
KONTROLLSKRIPT
==============
Gruppen-Id: pra02
Namen: Andreas Georg Junge, Ekrem Altuntop
====================================================================
Start in a directory with only the z1_a1 executable and this script.
"""
import numpy as np
import subprocess as sp
import time
import math
import matplotlib.pyplot as plt
import pylab as pb


#Storage variables
N_arr = 10**np.arange(2,7,dtype=int) #N values
pi_arr = np.array([], dtype=float) #pi values
tottime_arr = np.array([], dtype=float) #total program time
inittime_arr = np.array([], dtype=float) #time to init
comptime_arr = np.array([], dtype=float) #time for loop



#Submit jobs
##################################################

print("Submitting jobs now...")
for num in N_arr:
	sp.call("sbatch -n 1 --wrap \"./z1_a1 %s\" " %(str(num)), shell=True)
	time.sleep(1) #Entlastung fuer Slurm
print("Done.")

#get list of slurm files
##################################################
#NOTE:	This part has been strongly modified in order to somewhat work. 
#		Experimental setups are not included in this submission. 
print("Fetch values out of slurm-output-files...")
print("Wait a while to make sure, the files are here...")
time.sleep(20) #20 sec
ls_slurm = ""
ls_slurm = sp.check_output("ls slurm*", shell=True).decode("utf-8")
ls_slurm = ls_slurm.strip("\n").split("\n")
print("Files found.")
#Get the values and benchmarks
for filename in ls_slurm:
	with open(filename) as file:
		lines = file.readlines()
		pi_arr = np.append(pi_arr , float(lines[0].strip("Pi-Value:"))) #(hier: Double-Precision)
		tottime_arr = np.append(tottime_arr , float(lines[1].strip("Total Time:")))
		inittime_arr = np.append(inittime_arr , float(lines[2].strip("Init Time:")))
		comptime_arr = np.append(comptime_arr, float(lines[3].strip("Comp Time:")))

print("Done.")


#Absolute errors and conversions of units
##################################################
print("Calculate absolute differences and misc. unit conversions...")
diff_arr = np.abs(math.pi - pi_arr)
tottime_arr = tottime_arr / 1000
inittime_arr =  inittime_arr / 1000
comptime_arr = comptime_arr / 1000
print("Done.")


#Plots and display of work
##################################################
print("Plotting and printing results...")
plt.semilogx(N_arr, diff_arr, 'o-', color='red',  label="Absolute Fehler")
plt.title("Approximation von Pi - Absolute Abweichungen von Wahrheit")
plt.xlabel("N")
plt.legend()
plt.grid(True)
plt.show()
pb.savefig("z1_a1_fehler.png", dpi=600)
plt.close()

plt.semilogx(N_arr, pi_arr, 'o-', label="Berechnete Werte")
plt.semilogx(N_arr, np.ones(len(N_arr), dtype=float)*math.pi, '--', color='black', label="Wahrheit")
plt.title("Berechnete Approximationen von Pi")
plt.xlabel("N")
plt.legend()
plt.grid(True)
plt.show()
pb.savefig("z1_a1_werte.png", dpi=600)
plt.close()

plt.loglog(N_arr, tottime_arr, 'o-', label="Gesamtlaufzeit")
plt.loglog(N_arr, inittime_arr, 'o-', label="Start-Variablen Setzen")
plt.loglog(N_arr, comptime_arr, 'o-', label="Berechnungen in For-Schleife")
plt.title("Approximation von Pi - Zeit-Benchmarks")
plt.xlabel("N")
plt.ylabel("Zeit [µs]")
plt.legend()
plt.grid(True)
plt.show()
pb.savefig("z1_a1_bench.png", dpi=600)
plt.close()

print("N values:")
print(N_arr)
print("Real Pi value:")
print(math.pi)
print("Pi approximations:")
print(pi_arr)
print("Absolute errors:")
print(diff_arr)
print("Benchmark: Gesamtlaufzeit")
print(tottime_arr)
print("Benchmark: Laufzeit für Setzen von Start-Variablen")
print(inittime_arr)
print("Benchmark: Laufzeit für for-Schleife mit Berechnungen")
print(comptime_arr)


print("Done.")

print("Task terminated. Stop.")
